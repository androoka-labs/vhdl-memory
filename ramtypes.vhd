
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package ramtypes is
  component simple_dualport_ram is
    generic (
      DATA_WIDTH : integer := 32;
      ADDR_WIDTH : integer := 8;
      MEM_INIT_FILE : string := ""      
    );
    port (
      wr_clk : in std_logic;
      wr_data : in std_logic_vector(DATA_WIDTH-1 downto 0);
      wr_addr : in std_logic_vector(ADDR_WIDTH-1 downto 0);
      wr_en : in std_logic;
      rd_clk : in std_logic;
      rd_addr : in std_logic_vector(ADDR_WIDTH-1 downto 0);
      rd_q : out std_logic_vector(DATA_WIDTH-1 downto 0)
    );
  end component;

  end package;

