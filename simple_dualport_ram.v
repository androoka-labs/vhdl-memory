
module simple_dualport_ram(wr_clk,wr_data,wr_addr,wr_en,rd_clk,rd_addr,rd_q);
  parameter DATA_WIDTH = 32;
  parameter ADDR_WIDTH = 8;
  parameter MEM_INIT_FILE = "";

  output[DATA_WIDTH-1:0] rd_q;
  input [DATA_WIDTH-1:0] wr_data;
  input [ADDR_WIDTH-1:0] wr_addr;
  input [ADDR_WIDTH-1:0] rd_addr;
  input wr_en, wr_clk, rd_clk;

  reg [ADDR_WIDTH-1:0] addr_out_reg;
  reg [DATA_WIDTH-1:0] rd_q;
  (* ramstyle = "no_rw_check" *) reg [DATA_WIDTH-1:0] mem [(2**ADDR_WIDTH)-1:0];
 
  always @(posedge wr_clk) begin
    if (wr_en)
      mem[wr_addr] <= wr_data;
  end
  
  initial begin
    if (MEM_INIT_FILE != "") begin
      $readmemh(MEM_INIT_FILE, mem);
    end
  end

  always @(posedge rd_clk) begin
    rd_q <= mem[addr_out_reg];
    addr_out_reg <= rd_addr;
  end
	
endmodule


