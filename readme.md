

# VHDL-Memory  

This is simple example where I combine two common skills - verilog memory initialisation and vhdl/verilog cross language calling.  

---
### verilog base module  
> simple_dualport_ram.v  
>` (* ramstyle = "no_rw_check" *)` # tells Vivado/Quartus to use a typical memory style
> `   parameter MEM_INIT_FILE = ""; ` # define the memory file
>` $readmemh(MEM_INIT_FILE, mem);` # reads 
---
### vhdl test bench  
> simple_dualport_ram_tb.vhd  
>`  dut : simple_dualport_ram`  
>` generic map (`  
>`  ...`  
>`      MEM_INIT_FILE => "./mem.bin"`  
>`    )`  
---
### memory initisation file
the initialisation file is simple hex encoded text (readmem**h**) but other readmemb for binary. modelsim is tolerant on length, as long as the hex number can fit into the required widths.
> mem.bin  
>`0000`  
>`0100`  
>`2000`  
>`0000`  
>`0000`  
---

Example output using the testbench
![screenshot](screenshot.png)


