

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.ramtypes.all;

entity simple_dualport_ram_tb is
end;

architecture tb01 of simple_dualport_ram_tb is

  constant DW : integer := 16;
  constant AW : integer :=  4;

  signal clk : std_logic;
  signal wraddr,rdaddr : std_logic_vector(AW-1 downto 0);
  signal data,q : std_logic_vector(DW-1 downto 0);
  signal wren : std_logic := '1';

begin

  dut : simple_dualport_ram 
    generic map (
      ADDR_WIDTH => AW,
      DATA_WIDTH => DW,
      MEM_INIT_FILE => "./mem.bin"
    )
    port map (
      wr_clk => clk,
      wr_data => data,
      wr_addr => wraddr,
      wr_en => wren,
      rd_clk => clk,
      rd_addr => wraddr
      -- rd_q => q
    );

  process(clk)
    variable c : integer := 0;
  begin
    if rising_edge(clk) then
      case c is
      when 0 =>
        rdaddr <= (others => '0');
        wraddr <= (others => '0');
        wren   <= '0';
      when 40 =>
        wraddr <= x"e";
        data <= x"0123";
        wren <= '1';
      when others =>
        wren <= '0';
      end case;
      c := c + 1;

    end if;
  end process;

  process begin
    clk <= '0'; wait for 5 ns;
    clk <= '1'; wait for 5 ns;
  end process;


end;